<%-- 
    Document   : editEmployee
    Created on : Jun 25, 2018, 1:38:02 PM
    Author     : psonani
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="employee.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    ArrayList<Employee> empList = null;
    int empIdInt = 0;
    Employee employee = null;
    boolean allGood = true;

    //Check if employee list is not empty If empty than fill it.
    if (application.getAttribute("empListIndex") == null) {
        allGood = false;
    } else {
        empList = (ArrayList<Employee>) application.getAttribute("empListIndex");
    }

    //Check id empID is not null
    if (request.getParameter("empID") == null) {
        allGood = false;
    }

    //If parse empID into int
    if (allGood) {
        String empIdStr = request.getParameter("empID");
        try {
            empIdInt = Integer.parseInt(empIdStr);
        } catch (Exception e) {
            allGood = false;
        }
    }

    //check for empID is there
    if (allGood) {
        employee = new Employee();
        employee.setEmpID(empIdInt);

        if (empList.contains(employee)) {
            int index = empList.indexOf(employee);
            employee = empList.get(index);
        } else {
            allGood = false;
        }
    }

    // If there is error redirect into index.jsp
    if (!allGood) {
        response.sendRedirect("../index.jsp");
    } else {
        String[] positions = {"Boatswain", "Captain", "Carpenter", "Cooper", "Doctor", "First Mate", "Master Gunner", "Navigator", "Quarternaster", "Sailor"};


%>
<html>
    <head>
        <script type="text/javascript">

            function validateForm() {
                var error = false;
                var message = "";

                var value = document.forms["form1"]["empID"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Employee ID cannot be black! \n";
                }

                value = document.forms["form1"]["name"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Name cannot be blank! \n";
                }

                value = document.forms["form1"]["years"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Yeas cannot be blank! \n";
                }

                value = document.forms["form1"]["position"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Position cannot be blank! \n";
                }

                value = document.forms["form1"]["salary"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Salary cannot be blank! \n";
                }

                if (error) {
                    alert(message);
                    return false;
                } else {
                    return true;
                }
            }

        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/editEmployee.css">
        <title>Edit Employee</title>
    </head>
    <body>
        <div class="fullCard" id="thumbnail">
            <div class="cardContent">
                <div class="cardText">
                    <h4>Edit Employee <%= employee.getEmpID()%></h4>
                    <form method="GET" action="processEditEmployee.jsp" id="form1">
                        <table>
                            <tr>
                                <td class="ed-header">Id</td>
                                <td><input disabled="" type="text" value="<%= employee.getEmpID()%>"></td>
                            <input value="<%= employee.getEmpID()%>" name="empID" type="hidden">
                            </tr>
                            <!-- Check and retrieve Name from emp list-->
                            <tr>
                                <% if (request.getParameter("name") != null) { %>

                                <% if (request.getAttribute("nameError") != null) {%>
                                <td><font color="red">Name</font></td>
                                <td><input type="text" value="<%= employee.getName()%>" name="name"></td>
                                    <% } else {%>
                                <td>Name</td>
                                <td><input type="text" value="<%= request.getParameter("name")%>" name="name"></td>
                                    <% } %>
                                    <% } else {%>
                                <td class="ed-header">Name</td>
                                <td><input type="text" value="<%= employee.getName()%>" name="name"></td>
                                    <% } %>
                            </tr>
                            <!-- Check and retrieve years from emp list-->
                            <tr>                    
                                <% if (request.getParameter("years") != null) { %>                    
                                <% if (request.getAttribute("yearsError") != null) {%>
                                <td><font color="red">Years of Service</font></td>
                                <td><input type="text" value="<%= employee.getYearsOfService()%>" name="years"></td>
                                    <% } else {%>
                                <td class="ed-header">Years of Service</td>
                                <td><input type="text" value="<%= request.getParameter("years")%>" name="years"></td>
                                    <% } %>
                                    <% } else {%>
                                <td class="ed-header">Years of Service</td>
                                <td><input type="text" value="<%= employee.getYearsOfService()%>" name="years"></td>
                                    <% } %>

                            </tr>
                            <!-- Check and retrieve Position from emp list-->
                            <tr>
                                <td class="ed-header">Position</td>
                                <td>
                                    <select name="position">
                                        <% for (String position : positions) { %>
                                        <% if (position.equals(employee.getPosition())) {%>
                                        <option value="<%= position%>" selected><%= position%></option>
                                        <% } else {%>
                                        <option value="<%= position%>" ><%= position%></option>
                                        <% } %>
                                        <% } %>                            
                                    </select>                     
                                </td>                    
                            </tr>
                            <!-- Check and retrieve Salary from emp list-->
                            <tr>                   
                                <% if (request.getParameter("salary") != null) { %>

                                <% if (request.getAttribute("salaryError") != null) {%>
                                <td><font color="red">Salary</font></td>
                                <td><input type="text" value="<%= employee.getSalary()%>" name="salary"></td>
                                    <% } else {%>
                                <td>Salary</td>
                                <td><input type="text" value="<%= request.getParameter("salary")%>" name="salary"></td>
                                    <% } %>
                                    <% } else {%>
                                <td class="ed-header">Salary</td>
                                <td><input type="text" value="<%= employee.getSalary()%>" name="salary"></td>
                                    <% } %>                    
                            </tr>
                            <tr>
                                <td><input type="reset" value="Reset"></td>
                                <td><input type="submit" onclick="return validateForm()" value="Update"></td>                    
                            </tr>
                            <tr>
                                <td><a class="aempid-link addEmp" href="${pageContext.request.contextPath}/employee-view/employeeSortableList.jsp">Employee List</a></td>
                                <td><a class="aempid-link addEmp" href="${pageContext.request.contextPath}/index.jsp">Home</a></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<% }%>