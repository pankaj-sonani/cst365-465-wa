<%-- 
    Document   : processAddEmployee
    Created on : Jun 24, 2018, 2:53:55 PM
    Author     : psomani
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="employee.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
       
    if (application.getAttribute("empListIndex") == null) {
        response.sendRedirect("../index.jsp");
    } else {
        ArrayList<Employee> empList = (ArrayList<Employee>) application.getAttribute("empListIndex");

        boolean error = false;
        String empIDMessage = null;

        //check for parameters (empID)
        if (request.getParameter("empID") != null) {
            try {
                int empIDInt = Integer.parseInt(request.getParameter("empID"));

                //check if employee is already exists.
                Employee checkEmployee = new Employee();
                checkEmployee.setEmpID(empIDInt);

                if (empList.contains(checkEmployee)) {
                    error = true;
                    empIDMessage = "Employee ID already exists!";
                }
            } catch (Exception e) {
                error = true;
                empIDMessage = "Employee ID must be Integer";
            }
        } else {
            error = true;
            empIDMessage = "Employee ID cannot be black!";
        }

        //check Salary
        if (request.getParameter("salary") != null) {
            try {
                Double.parseDouble(request.getParameter("salary"));
            } catch (Exception e) {
                error = true;
            }
        } else {
            error = true;
        }

        //years of service
        if (request.getParameter("years") != null) {
            try {
                Integer.parseInt(request.getParameter("years"));
            } catch (Exception e) {
                error = true;
            }
        } else {
            error = true;

        }

        if ((request.getParameter("name") == null) || (request.getParameter("name").length() <= 0)) {
            error = true;
        }

        if ((request.getParameter("position") == null) || (request.getParameter("position").length() <= 0)) {
            error = true;
        }

        if (error) {
            String[] messages = new String[5];
            if (empIDMessage != null) {
                response.sendRedirect("addEmployee.jsp?empIDError=" + empIDMessage);
            } else {
                response.sendRedirect("addEmployee.jsp?empID=" + request.getParameter("empID"));
            }

        } else {
            //add Employee into List
            Employee employee = new Employee();
            employee.setEmpID(Integer.parseInt(request.getParameter("empID")));
            employee.setName(request.getParameter("name"));
            employee.setYearsOfService(Integer.parseInt(request.getParameter("years")));
            employee.setPosition(request.getParameter("position"));
            employee.setSalary(Double.parseDouble(request.getParameter("salary")));
            
            empList.add(employee);
            response.sendRedirect("../index.jsp");
        }
    }
%>

