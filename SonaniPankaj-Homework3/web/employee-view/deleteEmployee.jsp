<%-- 
    Document   : deleteEmployee
    Created on : Jun 25, 2018, 1:41:46 PM
    Author     : psonani
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="employee.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<Employee> empList = null;
    int empIdInt = 0;
    Employee employee = null;
    boolean allGood = true;
    
    //Check if employee list is not empty If empty than fill it.
    if(application.getAttribute("empListIndex") == null) {
        allGood = false;
    } else {
        empList = (ArrayList<Employee>) application.getAttribute("empListIndex");
    }
    
    //Check id empID is not null
    if(request.getParameter("empID") == null){
        allGood = false;
    }
    
    //If parse empID into int
    if(allGood) {
        String empIdStr = request.getParameter("empID");
        try {
            empIdInt = Integer.parseInt(empIdStr);
        } catch (Exception e){
            allGood = false;            
        }
    }
    
    //check for empID is there
    if (allGood){
        employee = new Employee();
        employee.setEmpID(empIdInt);
        
        if(empList.contains(employee)) {
            int index = empList.indexOf(employee);
            employee = empList.get(index);
        } else {
            allGood = false;
        }
    }
    
    // If there is error redirect into index.jsp
    if (!allGood) {
        response.sendRedirect("../index.jsp");
    } else {
        empList.remove(employee);
        response.sendRedirect("../index.jsp");
    }
    

%>