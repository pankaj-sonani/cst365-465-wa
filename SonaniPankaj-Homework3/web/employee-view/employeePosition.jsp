<%-- 
    Document   : index
    Created on : Jun 23, 2018, 1:27:01 PM
    Author     : psonani
--%>

<%@page import="employee.EmployeePositionComparator"%>
<%@page import="employee.Employee"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="employee.EmployeeUtil"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    EmployeeUtil employeeUtil = new EmployeeUtil();
    ArrayList<Employee> empList = employeeUtil.getEmployeeList();
    Collections.sort(empList, new EmployeePositionComparator());
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/index.css">
        <title>Employee List by Position</title>
    </head>
    <body>
        <h1>Employee Sorted by Position</h1>
        <table>
            <tr>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeeById.jsp">Id</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeeName.jsp">Name</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeeYears.jsp">Years Of Service</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeePosition.jsp">Position</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeeSalary.jsp">Salary</a></th>           
            </tr>
             <% for (Employee emp : empList) {%>
            <tr>
                <td><%= emp.getEmpID()%></td>
                <td><%= emp.getName()%></td>
                <td><%= emp.getYearsOfService()%></td>
                <td><%= emp.getPosition()%></td>
                <td><%= emp.getSalary()%></td>
            </tr>
            <% }%>
        </table>
    </body>
</html>
