package employee;


import employee.Employee;
import java.util.Comparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mruth
 */
public class EmployeePositionSalaryComparator implements Comparator<Employee>{

    @Override
    public int compare(Employee a, Employee b) {
        //if postions equal, compare salaries
        if (a.getPosition().compareTo(b.getPosition()) == 0) {
            //compare salaries
            if (a.getSalary() > b.getSalary()) {
                return 1; 
            } else if (a.getSalary() < b.getSalary()) {
                return -1;
            } else {
                return 0;
            }
            
        } else {
            //not equal so use positions
            return a.getPosition().compareTo(b.getPosition());
        }
        
    }
    
}
