/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pankaj sonani
 */
public class Employee implements Comparable<Employee> {

    private int employeeId;
    private String name;
    private int yearsOfService;
    private String position;
    private double salary;

    public Employee() {
    }

    public Employee(int employeeId, String name, int yearsOfService, String position, double salary) {
        this.employeeId = employeeId;
        this.name = name;
        this.yearsOfService = yearsOfService;
        this.position = position;
        this.salary = salary;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(Employee other) {
        return this.employeeId - other.employeeId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.employeeId;
        return hash;
    }

    @Override
    public String toString() {
        return "Employee{" + "employeeId=" + employeeId + ", name=" + name + ", yearsOfService=" + yearsOfService + ", position=" + position + ", salary=" + salary + '}';
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearsOfService() {
        return yearsOfService;
    }

    public void setYearsOfService(int yearsOfService) {
        this.yearsOfService = yearsOfService;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

}
