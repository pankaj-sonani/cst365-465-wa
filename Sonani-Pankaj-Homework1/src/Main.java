
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mruth
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //set up array for first names (first names of all new orleans saints)
        String[] fnames = {"Alex", "Terron", "Dan", "Chris", "J.T.", "Vonn", "Drew", "Jay",
            "Jermon", "Austin", "Will", "Kurt", "Brandon", "Ken", "Marcus",
            "Demario", "Tyeler", "Travin", "Trey", "Jayrone", "KeShun",
            "John", "Ted", "Garrett", "Woodrow", "Justin", "De'Vante",
            "Cory", "Josh", "Taysom", "Michael", "Josh", "Mark", "Natrell",
            "George", "Cameron", "Colton", "Alvin", "Hau'oli", "Keith",
            "A.J.", "Daniel", "Marshon", "Devaroe", "Rick", "Tommylee",
            "Zach", "Mitchell", "Wil", "Eldridge", "Arthur", "Henry",
            "Kamrin", "Thomas", "Al-Quadin", "David", "Andrus", "Ryan",
            "Sheldon", "Craig", "Patrick", "Tom", "Boston", "Tre'Quan",
            "Taylor", "Linden", "Nate", "Manti", "Mykkele", "Andrew",
            "Paul", "Landon", "Max", "Larry", "Ben", "P.J.", "Jonathan", "Pankaj",
            "Deon"};
        //setup array for last names (last names of all US presidents)
        String[] lnames = {"Trump", "Obama", "Bush", "Clinton", "Reagan", "Carter", "Ford",
            "Nixon", "Johnson", "Kennedy", "Eisenhower", "Truman", "Roosevelt",
            "Hoover", "Coolidge", "Harding", "Wilson", "Taft", "McKinley",
            "Cleveland", "Harrison", "Arthur", "Garfield", "Hayes", "Grant",
            "Lincoln", "Buchanan", "Pierce", "Fillmore", "Taylor", "Polk",
            "Tyler", "Van Buren", "Jackson", "Adams", "Monroe", "Madison", "Sonani",
            "Jefferson", "Washington"};
        //setup array for positions -> positions/jobs on a pirate vessel
        String[] positions = {"Captain", "Navigator", "Quarternaster", "Boatswain",
            "Cooper", "Carpenter", "Doctor", "Master Gunner", "First Mate",
            "Sailor"};

        Random random = new Random();

        List<Employee> employees = new ArrayList();

        for (int i = 0; i < 50; i++) {

            //employee ID -  6 digit number
            int empID = random.nextInt(89999) + 10000;
            //Name – String
            String name = lnames[random.nextInt(lnames.length)] + ", " + fnames[random.nextInt(fnames.length)];
            // Years of Service – int - 0 to 30
            int yos = random.nextInt(30);
            //Position – String 
            String position = positions[random.nextInt(positions.length)];
            //Salary – double 30,000 to 500,000
            double sal = random.nextDouble() * 470000 + 30000;

            Employee employee = new Employee();
            //employee.setEmployeeId((int)(Math.random()*90000+100));
            employee.setEmployeeId(empID);
            employee.setName(name);
            employee.setPosition(position);
            employee.setSalary(sal);
            employee.setYearsOfService(yos);

            employees.add(employee);

        }

        System.out.println("Employees list.size(): " + employees.size());

        for (Object obj : employees) {
            if (obj instanceof Employee) {
                Employee emp = (Employee) obj;
                System.out.println(emp);
            } else {
                System.out.println(obj);
            }
        }

        //Setting first elements to EmployeeID 90210  --- This is only for testing 
        for (int i = 0; i < employees.size(); i++) {
            Employee employee = (Employee) employees.get(0);
            employee.setEmployeeId(90210);
        }

        System.out.println("\nFinding Employee Id 90210");
        for (int i = 0; i < employees.size(); i++) {
            Employee employee = (Employee) employees.get(i);
            if (employee.getEmployeeId() == 90210) {
                System.out.print("Found Employee ID 90210 :- ");
                System.out.println(employee);
                break;
            } else {
                System.out.println("No Employee Id 90210 Found!!");
            }
        }

        //Setting first elements to Employee Name  “Drew, Brees --- This is only for testing 
        for (int i = 0; i < employees.size(); i++) {
            Employee employee = (Employee) employees.get(0);
            employee.setName("“Drew, Brees");
        }

        System.out.println("\nFinding Employee Name  “Drew Brees");
        for (int i = 0; i < employees.size(); i++) {
            Employee employee = (Employee) employees.get(i);
            if (employee.getName() == "“Drew, Brees") {
                System.out.print("Found Employee Name “Drew Brees :- ");
                System.out.println(employee);
                break;
            } else {
                System.out.println("No Employee Name “Drew Brees\" Found!!");
            }
        }

        System.out.println("\nFinding Employee who have been employed more than 10 years");
        for (int i = 0; i < employees.size(); i++) {
            Employee employee = (Employee) employees.get(i);
            if (employee.getYearsOfService() > 10) {
                System.out.print("Found Employee who servers more than 10 years ");
                System.out.println(employee);
            }
        }

        System.out.println("\nNatural order of employees by ID");
        Collections.sort(employees);

        for (Employee employee : employees) {
            System.out.println(employee);
        }

        System.out.println("\nSort the list using employee names using a comparator and print the results");
        Collections.sort(employees, new EmployeeNameComparator());

        for (Employee employee : employees) {
            System.out.println(employee);
        }

        System.out.println("\nSort the list using employee positions, then salaries using a comparator and print the results");

        Comparator[] comparators = new Comparator[2];
        comparators[1] = new EmployeePositionsComparator();
        comparators[0] = new EmployeeSalaryComparator();
        //List<Employee> sortedEmployee = new ArrayList<>();
        for (Comparator c : comparators) {

            System.out.println(c.getClass());

            Collections.sort(employees, c);

        }
        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }

}
