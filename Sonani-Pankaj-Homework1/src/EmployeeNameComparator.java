
import java.util.Comparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author psomani
 */
public class EmployeeNameComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
       String[] firstName = o1.getName().split(", ");
       String[] lastName = o2.getName().split(", ");
       
       if(firstName[1].compareTo(lastName[1]) == 0) {
           
           return o1.getName().compareTo(o2.getName());
       }else {
           return firstName[1].compareTo(lastName[1]);
       }
    }
    
}
