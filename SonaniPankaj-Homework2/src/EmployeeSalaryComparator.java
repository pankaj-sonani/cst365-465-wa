
import java.util.Comparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author merut
 */
public class EmployeeSalaryComparator implements Comparator<Employee>{

    @Override
    public int compare(Employee a, Employee b) {
        if (a.getSalary() > b.getSalary()) {
            return 1;
        } else if (a.getSalary() < b.getSalary()) {
            return -1;
        } else {
            return 0;
        }
        
    }
    
}
