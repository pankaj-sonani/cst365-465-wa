/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mruth
 */
public class Employee implements Comparable<Employee>{

    private int empID;
    
    private String name;
    
    private int yearsOfService;
    
    private String position;
    
    private double salary;

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearsOfService() {
        return yearsOfService;
    }

    @Override
    public String toString() {
        return empID + "\t" + name + "\t"  + yearsOfService + "\t" + position + "\t" + salary;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.empID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (this.empID != other.empID) {
            return false;
        }
        return true;
    }

    public Employee(int empID, String name, int yearsOfService, String position, double salary) {
        this.empID = empID;
        this.name = name;
        this.yearsOfService = yearsOfService;
        this.position = position;
        this.salary = salary;
    }

    public void setYearsOfService(int yearsOfService) {
        this.yearsOfService = yearsOfService;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    
    
    
    @Override
    public int compareTo(Employee other) {
        return this.empID - other.getEmpID();
    }
    
}
