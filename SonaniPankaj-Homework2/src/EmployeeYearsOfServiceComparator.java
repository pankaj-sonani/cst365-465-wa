
import java.util.Comparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author merut
 */
public class EmployeeYearsOfServiceComparator implements Comparator<Employee>{

    @Override
    public int compare(Employee a, Employee b) {
        return a.getYearsOfService() - b.getYearsOfService();
    }
    
}
