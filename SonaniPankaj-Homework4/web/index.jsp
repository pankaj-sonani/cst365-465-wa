<%-- 
    Document   : index
    Created on : Jun 23, 2018, 1:27:01 PM
    Author     : psonani
--%>

<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Random"%>
<%@page import="user.User"%>
<%@page import="employee.EmployeeSalaryComparator"%>
<%@page import="employee.EmployeePositionComparator"%>
<%@page import="employee.EmployeeYearsOfServiceComparator"%>
<%@page import="employee.EmployeeNameComparator"%>
<%@page import="employee.Employee"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="employee.EmployeeUtil"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
  
 
    ArrayList<Employee> empListIndex = null;
    if (application.getAttribute("empListIndex") != null) {
        empListIndex = (ArrayList<Employee>) application.getAttribute("empListIndex");
    } else {
        empListIndex = new ArrayList();

        //set up array for first names (first names of all new orleans saints)
        String[] fnames = {"Alex", "Terron", "Dan", "Chris", "J.T.", "Vonn", "Drew", "Jay",
            "Jermon", "Austin", "Will", "Kurt", "Brandon", "Ken", "Marcus",
            "Demario", "Tyeler", "Travin", "Trey", "Jayrone", "KeShun",
            "John", "Ted", "Garrett", "Woodrow", "Justin", "De'Vante",
            "Cory", "Josh", "Taysom", "Michael", "Josh", "Mark", "Natrell",
            "George", "Cameron", "Colton", "Alvin", "Hau'oli", "Keith",
            "A.J.", "Daniel", "Marshon", "Devaroe", "Rick", "Tommylee",
            "Zach", "Mitchell", "Wil", "Eldridge", "Arthur", "Henry",
            "Kamrin", "Thomas", "Al-Quadin", "David", "Andrus", "Ryan",
            "Sheldon", "Craig", "Patrick", "Tom", "Boston", "Tre'Quan",
            "Taylor", "Linden", "Nate", "Manti", "Mykkele", "Andrew",
            "Paul", "Landon", "Max", "Larry", "Ben", "P.J.", "Jonathan",
            "Deon"};
        //setup array for last names (last names of all US presidents)
        String[] lnames = {"Trump", "Obama", "Bush", "Clinton", "Reagan", "Carter", "Ford",
            "Nixon", "Johnson", "Kennedy", "Eisenhower", "Truman", "Roosevelt",
            "Hoover", "Coolidge", "Harding", "Wilson", "Taft", "McKinley",
            "Cleveland", "Harrison", "Arthur", "Garfield", "Hayes", "Grant",
            "Lincoln", "Buchanan", "Pierce", "Fillmore", "Taylor", "Polk",
            "Tyler", "Van Buren", "Jackson", "Adams", "Monroe", "Madison",
            "Jefferson", "Washington"};
        //setup array for positions -> positions/jobs on a pirate vessel
        String[] positions = {"Captain", "Navigator", "Quarternaster", "Boatswain",
            "Cooper", "Carpenter", "Doctor", "Master Gunner", "First Mate",
            "Sailor"};

        ArrayList<User> users = new ArrayList();       
             
        if (application.getAttribute("users") != null) {
            users = (ArrayList<User>) application.getAttribute("users");
        } else {
            //Create administrator
            User administrator = new User();
            administrator.setRealName("Administrator");
            administrator.setUserName("admin");
            administrator.setPassword("admin");
            administrator.setIsAdmin(true);
            users.add(administrator);

            //Create 3 supervisors
            String supRealName = "Supervisors 00";
            String supUserIdAndPassword = "sup";

            for (int i = 1; i <= 3; i++) {
                User supervisors = new User();
                supervisors.setRealName(supRealName + i);
                supervisors.setUserName(supUserIdAndPassword + i);
                supervisors.setPassword(supUserIdAndPassword + i);
                supervisors.setIsAdmin(false);

                users.add(supervisors);
            }

            application.setAttribute("users", users);

        }
        
        Random random = new Random();
        int num = 50;

        for (int i = 0; i < num; i++) {
            //employee ID -  6 digit number
            int empID = random.nextInt(89999) + 10000;
            //Name – String
            String name = lnames[random.nextInt(lnames.length)] + ", " + fnames[random.nextInt(fnames.length)];
            // Years of Service – int - 0 to 30
            int yos = random.nextInt(30);
            //Position – String 
            String position = positions[random.nextInt(positions.length)];
            //Salary – double 30,000 to 500,000
            double sal = random.nextDouble() * 470000 + 30000;

            //create an employee
            Employee e = new Employee(empID, name, yos, position, sal);

            users = (ArrayList<User>) application.getAttribute("users");
           
            e.setSupervisor(users.get((int) (Math.random() * users.size())));
            
            
            empListIndex.add(e);
        }
        application.setAttribute("empListIndex", empListIndex);
    }

   if (session.getAttribute("login") == null || session.getAttribute("login").equals("")) {
        //Send user to login 
        response.sendRedirect(request.getContextPath() + "/security/login.jsp");

    } else {
        User user = (User) session.getAttribute("login");

        ArrayList<Employee> showables = new ArrayList();

        for (Employee emp : empListIndex) {
            if ((emp.getSupervisor().equals(user)) || (user.getIsAdmin())) {
                showables.add(emp);
            }
        }

        if (request.getParameter("sort") == null) {
            Collections.sort(showables);
        } else {

            String sort = request.getParameter("sort");
            if (sort.equals("name")) {
                Collections.sort(showables, new EmployeeNameComparator());
            } else if (sort.equals("years")) {
                Collections.sort(showables, new EmployeeYearsOfServiceComparator());
            } else if (sort.equals("positions")) {
                Collections.sort(showables, new EmployeePositionComparator());
            } else {
                Collections.sort(showables, new EmployeeSalaryComparator());
            }
        }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/index.css">
        <title>Employee List</title>
    </head>
    <body>
        <h1>Employee List For <%= user.getRealName()%></h1>      
        <table>
            <tr>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp">Id</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=name">Name</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=years">Years of Service</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=positions">Positions</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=salary">Salary</a></th>
                <% if (user.getIsAdmin()) { %>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=name">Supervisor</a></th>
                  <% } %>
                <th colspan="2">Actions</th>                
            </tr>
            <% for (Employee emp : showables) {%>
            <tr>
                <td><a class="aempid-link" href="${pageContext.request.contextPath}/employee-view/employeeDetail.jsp?empID=<%= emp.getEmpID()%>"><%= emp.getEmpID()%></a></td>            
                <td><%= emp.getName()%></td>
                <td><%= emp.getYearsOfService()%></td>
                <td><%= emp.getPosition()%></td>
                 <% NumberFormat fmt = NumberFormat.getCurrencyInstance(); %>
                <td><%= fmt.format(emp.getSalary())%></td>
                <% if (user.getIsAdmin()) { %>
                <td><a class="aempid-link" href="${pageContext.request.contextPath}/employee-view/viewBySupervisor.jsp?supID=<%= emp.getSupervisor().getRealName() %>"><%= emp.getSupervisor().getRealName()%></td>
                <% } %>
                <td><a href="${pageContext.request.contextPath}/employee-view/editEmployee.jsp?empID=<%= emp.getEmpID()%>"><img height="20" width="20" src="${pageContext.request.contextPath}/images/edit.png" border="0"></a></td>
                <td><a href="${pageContext.request.contextPath}/employee-view/deleteEmployee.jsp?empID=<%= emp.getEmpID()%>"><img height="20" width="20" src="${pageContext.request.contextPath}/images/delete3.png" border="0"></a></td>

            </tr>
            <% }%>
        </table>
        <br>        
        <a class="aempid-link addEmp" href="${pageContext.request.contextPath}/employee-view/addEmployee.jsp">Add Employee</a>
        <a class="aempid-link addEmp" href="${pageContext.request.contextPath}/security/logout.jsp">Logout</a>
        <% if (user.getIsAdmin()) { %>
        <a class="aempid-link addEmp" href="${pageContext.request.contextPath}/employee-view/employeeSortableList.jsp">View Sortable List</a>
        <a class="aempid-link addEmp" href="security/manageUsers.jsp">Manage Users</a><% }%>
        <a class="aempid-link addEmp" href="security/editUser.jsp?un=<%= user.getUserName()%>">Manage Account</a>
    </body>
</html>
<% }%>