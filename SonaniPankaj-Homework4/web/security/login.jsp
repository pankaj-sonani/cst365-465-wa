<%-- 
    Document   : login
    Created on : Jun 28, 2018, 3:39:55 PM
    Author     : psonani
--%>

<%@page import="user.User"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("login") != null) {
        //User is already logged in
        response.sendRedirect("../index.jsp");
    } else {
        ArrayList<User> users = new ArrayList();
        // If User exists       
        if (application.getAttribute("users") != null) {
            users = (ArrayList<User>) application.getAttribute("users");
        } else {
            //Create administrator
            User administrator = new User();
            administrator.setRealName("administrator");
            administrator.setUserName("admin");
            administrator.setPassword("admin");
            administrator.setIsAdmin(true);
            users.add(administrator);

            //Create 3 supervisors
            String supRealName = "Supervisors 00";
            String supUserIdAndPassword = "sup";

            for (int i = 1; i <=3; i++) {
                User supervisors = new User();
                supervisors.setRealName(supRealName + i);
                supervisors.setUserName(supUserIdAndPassword + i);
                supervisors.setPassword(supUserIdAndPassword + i);
                supervisors.setIsAdmin(false);

                users.add(supervisors);
            }

            application.setAttribute("users", users);

        }

        if (request.getParameter("user") == null) { %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/addEmployee.css">

        <title>Login Page</title>
    </head>
    <body>
        <div class="fullCard" id="thumbnail">
            <div class="cardContent">
                <div class="cardText">
                    <form id="login" action="login.jsp" method="POST">
                        <table>
                            <tr>
                                <td class="ed-header">User Name</td>
                                <td>
                                    <input name="user" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="ed-header">Password</td>
                                <td>
                                    <input name="password" type="password">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="submit" value="login">
                                </td>
                            </tr>
                        </table>            
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<%  } else {

    boolean error = false;

    if ((request.getParameter("user") != null) && (request.getParameter("password") != null)) {
        if (request.getParameter("user").equals("")) {
            error = true;
            request.setAttribute("user", "User cannot be blank");
        } else {
            //Check User Name there?
            User fake = new User();
            fake.setUserName(request.getParameter("user"));
            if (users.contains(fake)) {
                //Get real User
                int index = users.indexOf(fake);
                User user = users.get(index);
                //Check Password
                if (user.getPassword().equals(request.getParameter("password"))) {
                    //set a seesion attribute
                    session.setAttribute("login", user);
                    //go to index page
                    response.sendRedirect("../index.jsp");

                } else if (request.getParameter("password").equals("")) {
                    error = true;
                    request.setAttribute("password", "Password cannot be blank");

                } else {

                    error = true;
                    request.setAttribute("password", "Bad Password");
                }
            } else {
                error = true;
                request.setAttribute("user", "Bad User");
            }
        }
    } else {
        response.sendRedirect("login.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/addEmployee.css">

        <title>Login Page</title>
    </head>
    <body>
        <div class="fullCard" id="thumbnail">
            <div class="cardContent">
                <div class="cardText">
                        <form id="login" action="login.jsp" method="POST">
                        <table>
                            <tr>
                                <% if (request.getAttribute("user") != null) {%>
                                <td class="ed-header"><font color="red">User Name (<%= request.getAttribute("user")%>)</td>
                                <td><input name="user" type="text"></td> 
                                    <% } else {%>
                                <td class="ed-header">User Name</td>
                                <td><input name="user" value="<%= request.getParameter("user")%>" type="text"></td> 
                                    <% } %>
                            </tr>
                            <tr>
                                <% if (request.getAttribute("password") != null) {%>
                                <td class="ed-header"><font color="red">Password (<%= request.getAttribute("password")%>)</td>
                                    <% } else { %>
                                <td class="ed-header">Password</td>
                                <% } %>
                                <td><input name="password" type="password"></td> 
                            </tr>
                            <tr><td colspan="2"><input type="submit" value="Login"></td></tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<% }
    }%>
