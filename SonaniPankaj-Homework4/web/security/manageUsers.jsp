<%-- 
    Document   : manageUsers
    Created on : Jun 30, 2018, 6:25:22 PM
    Author     : pankajsonani
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="user.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if(session.getAttribute("login") == null) {
        response.sendRedirect("login.jsp");
    } else {
        User user = (User)session.getAttribute("login");
        if(!user.getIsAdmin()){
            response.sendRedirect("../index.jsp");
        } else {
            ArrayList<User> users = (ArrayList<User>)application.getAttribute("users");
     
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/index.css">
        <title>User List</title>
    </head>
    <body>
        <h1>User List</h1>
        <table>
            <tr>
                <th>User Name</th>
                <th>Real Name</th>
                <th>Position</th>
                <th>Administrator?</th>
            </tr>
            <% for (User u : users) {%>
            <tr>
               <td><a class="aempid-link" href="editUser.jsp?un=<%= u.getUserName() %>"><%= u.getUserName() %></a></td>
                <td><%= u.getRealName() %></td>
                <td><%= u.getPosition() %></td>
                <td><%= u.getIsAdmin() ?"X":"&nbsp;" %></td>
            </tr>
            <% } %>
        </table>
        <br>
        <a class="aempid-link addEmp" href="../index.jsp">Go Home!</a>
        <a class="aempid-link addEmp" href="logout.jsp">Logout</a>

    </body>
</html>
<% } }%>