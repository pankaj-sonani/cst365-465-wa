<%-- 
    Document   : editUser
    Created on : Jun 30, 2018, 5:43:19 PM
    Author     : pankajsonani
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="user.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("login") == null) {
        //user is already logged in
        response.sendRedirect("login.jsp");
    } else {
        User user = (User) session.getAttribute("login");
        ArrayList<User> users = (ArrayList<User>) application.getAttribute("users");
        String un = "";

        User u = null;

        boolean allgood = true;

        if (request.getParameter("un") == null) {
            allgood = false;
        }

        if (allgood) {
            un = request.getParameter("un");
            u = new User();
            u.setUserName(un);
            if (users.contains(u)) {
                int index = users.indexOf(u);
                u = users.get(index);
            } else {
                allgood = false;
            }
        }

        if (allgood) {
            if (!((user.equals(u)) || (user.getIsAdmin()))) {
                allgood = false;
            }

        }

        if (!allgood) {
            if (user.getIsAdmin()) {
                response.sendRedirect("manageUsers.jsp");
            } else {
                response.sendRedirect("../index.jsp");
            }

        } else {

            String[] positions = {"Boatswain", "Captain", "Carpenter", "Cooper", "Doctor", "First Mate", "Master Gunner", "Navigator", "Quarternaster", "Sailor"};


%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit User</title>
    </head>
    <body>
        <h1>User (<%= u.getUserName()%>)</h1>
        <form action="processEditUser.jsp" method="POST">
            <input type="hidden" name="un" value="<%= u.getUserName() %>">
            Username: <input type="text" disabled value="<%= u.getUserName() %>"><br>

            Password:<input type="password" name="pass1" value="<%= u.getPassword()%>"><br>
            repeat Password:<input type="password" name="pass2" value="<%= u.getPassword()%>"><br>

            <% if (user.getIsAdmin() ) { %>
            Position: <select name="position">
                <% for (String position : positions) { %>
                <% if (position.equals(u.getPosition())) {%>
                <option value="<%= position %>" selected><%= position %></option>
                <% } else {%>
                <option value="<%= position %>" ><%= position %></option>
                <% } %>
                <% } %>

            </select><br>
            <% if (u.getIsAdmin()) { %>
            Supervisor: <input checked type="checkbox" name="administrator"><br>
            <% } else { %>
            Supervisor: <input type="checkbox" name="administrator"><br>
            <% } %>
            <% } %>

            <input type="submit" value="Edit User">
        </form>
    </body>
</html>
<% } } %>