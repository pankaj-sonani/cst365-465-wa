<%-- 
    Document   : logout
    Created on : Jun 30, 2018, 11:20:15 AM
    Author     : pankajsonani
--%>

<% 
        //Clear out session
        session.invalidate();
        
        // redirect to login page
        response.sendRedirect("login.jsp");
        
%>