<%-- 
    Document   : EmployeeDetail
    Created on : Jun 23, 2018, 4:48:26 PM
    Author     : psomani
--%>

<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="employee.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    if (session.getAttribute("login") == null) {
        //user is already logged in
        response.sendRedirect("login.jsp");

    } else {
        ArrayList<Employee> empListIndex = null;
        int empId = 0;
        Employee employee = null;
        boolean isEmaployeeAvailable = true;

        if (application.getAttribute("empListIndex") == null) {
            isEmaployeeAvailable = false;
        } else {
            empListIndex = (ArrayList<Employee>) application.getAttribute("empListIndex");
        }

        if (request.getParameter("empID") == null) {
            isEmaployeeAvailable = false;
        }

        if (isEmaployeeAvailable) {
            String empIDStr = request.getParameter("empID");
            try {
                empId = Integer.parseInt(empIDStr);
            } catch (Exception e) {
                isEmaployeeAvailable = false;
            }
        }

        //Check for employee availability 
        if (isEmaployeeAvailable) {
            employee = new Employee();
            employee.setEmpID(empId);

            if (empListIndex.contains(employee)) {
                int index = empListIndex.indexOf(employee);
                employee = empListIndex.get(index);
            } else {
                isEmaployeeAvailable = false;
            }
        }

        if (!isEmaployeeAvailable) {
            response.sendRedirect("index.jsp");
        } else {


%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/employeeDetail.css">
        <title>Show Employee (<%= employee.getEmpID()%> </title>
    </head>
    <body>
        <div class="fullCard" id="thumbnail">
            <div class="cardContent">
                <div class="cardText">
                    <h4>View Employee <%= employee.getEmpID()%></h4>
                    <table>
                        <tr>
                            <td class="ed-header">Name: </td><td><%= employee.getName()%></td>                    
                        </tr>
                        <tr>
                            <td class="ed-header">Years: </td><td><%= employee.getYearsOfService()%></td>                    
                        </tr>
                        <tr>
                            <td class="ed-header">Position:  </td><td><%= employee.getPosition()%></td>                    
                        </tr>
                        <tr>                      
                            <% NumberFormat fmt = NumberFormat.getCurrencyInstance(); %>                      
                            <td class="ed-header">Salary: </td><td><%= fmt.format(employee.getSalary()) %></td>                    
                        </tr>
                        <tr class="remove-border">
                            <td><a class="aempid-link addEmp" href="deleteEmployee.jsp?empID=<%= employee.getEmpID()%>">Delete</a></td>
                            <td><a class="aempid-link addEmp" href="editEmployee.jsp?empID=<%= employee.getEmpID()%>">Edit</a></td>                    
                        </tr>
                        <tr>
                            <td colspan="2"><a class="aempid-link addEmp" href="${pageContext.request.contextPath}/employee-view/addEmployee.jsp">Add Employee</a></td>
                        </tr>
                        <tr class="remove-border">                            
                            <td><a class="aempid-link addEmp" href="${pageContext.request.contextPath}/index.jsp">Home</a></td>
                        </tr>
                    </table>                    
                </div>
            </div>
        </div>
    </body>
</html>
<% }
    }%>