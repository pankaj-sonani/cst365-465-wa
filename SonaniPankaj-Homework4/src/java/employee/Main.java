package employee;


import employee.Employee;
import employee.EmployeeNameComparator;
import employee.EmployeePositionSalaryComparator;
import employee.EmployeeSalaryComparator;
import employee.EmployeeYearsOfServiceComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mruth
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //set up array for first names (first names of all new orleans saints)
        String[] fnames = {"Alex", "Terron", "Dan", "Chris", "J.T.", "Vonn", "Drew", "Jay",
            "Jermon", "Austin", "Will", "Kurt", "Brandon", "Ken", "Marcus",
            "Demario", "Tyeler", "Travin", "Trey", "Jayrone", "KeShun",
            "John", "Ted", "Garrett", "Woodrow", "Justin", "De'Vante",
            "Cory", "Josh", "Taysom", "Michael", "Josh", "Mark", "Natrell",
            "George", "Cameron", "Colton", "Alvin", "Hau'oli", "Keith",
            "A.J.", "Daniel", "Marshon", "Devaroe", "Rick", "Tommylee",
            "Zach", "Mitchell", "Wil", "Eldridge", "Arthur", "Henry",
            "Kamrin", "Thomas", "Al-Quadin", "David", "Andrus", "Ryan",
            "Sheldon", "Craig", "Patrick", "Tom", "Boston", "Tre'Quan",
            "Taylor", "Linden", "Nate", "Manti", "Mykkele", "Andrew",
            "Paul", "Landon", "Max", "Larry", "Ben", "P.J.", "Jonathan",
            "Deon"};
        //setup array for last names (last names of all US presidents)
        String[] lnames = {"Trump", "Obama", "Bush", "Clinton", "Reagan", "Carter", "Ford",
            "Nixon", "Johnson", "Kennedy", "Eisenhower", "Truman", "Roosevelt",
            "Hoover", "Coolidge", "Harding", "Wilson", "Taft", "McKinley",
            "Cleveland", "Harrison", "Arthur", "Garfield", "Hayes", "Grant",
            "Lincoln", "Buchanan", "Pierce", "Fillmore", "Taylor", "Polk",
            "Tyler", "Van Buren", "Jackson", "Adams", "Monroe", "Madison",
            "Jefferson", "Washington"};
        //setup array for positions -> positions/jobs on a pirate vessel
        String[] positions = {"Captain", "Navigator", "Quarternaster", "Boatswain",
            "Cooper", "Carpenter", "Doctor", "Master Gunner", "First Mate",
            "Sailor"};
        
        String[] supervisor = {"supervisors001","supervisors002","supervisors003"};

        Random random = new Random();

        //create an array list that can only hold employees
        ArrayList<Employee> list = new ArrayList<Employee>();
        //need 50 of them
        for (int i = 0; i < 50; i++) {

            //employee ID -  6 digit number
            int empID = random.nextInt(89999) + 10000;
            //Name – String
            String name = lnames[random.nextInt(lnames.length)] + ", " + fnames[random.nextInt(fnames.length)];
            // Years of Service – int - 0 to 30
            int yos = random.nextInt(30);
            //Position – String 
            String position = positions[random.nextInt(positions.length)];
            //Salary – double 30,000 to 500,000
            double sal = random.nextDouble() * 470000 + 30000;
            
             String sup = supervisor[random.nextInt(supervisor.length)];
            //create an employee
            Employee e = new Employee(empID,name,yos,position,sal);
            //add to list
            list.add(e);
            
        }
        
        //look for an employee with ID 90210
        //create a fake employee
        Employee fake = new Employee(90210,"",0,"",0.0);
        //now we can use contains for the check
        if (list.contains(fake)) {
            System.out.println("List contains employee with 90210 as their ID");
        } else {
            System.out.println("List DOES NOT contain employee with 90210 as their ID");
        }
        System.out.println("");
        //now we can check for name "Brees, Drew"
        boolean present = false;
        //we have to loop though
        for (int i=0; i<list.size(); i++) {
            //get the name of the item at i
            String name = list.get(i).getName();
            //is the name drew brees?
            if (name.equals("Brees, Drew")) {
                present = true;
            }
        }
        //now we can use the variable
        if (present) {
            System.out.println("Drew Brees is present in the list");
        } else {
            System.out.println("Drew Brees is NOT present in the list");
        }
        System.out.println("");
        //now we need to loop again
        for (Employee e: list) {
            //check for yos > 10
            if (e.getYearsOfService() > 10) {
                //print id and name
                System.out.println(e.getEmpID() + "\t" + e.getName());
            }
          
        }
        System.out.println("");
        //sort the list
        Collections.sort(list);
        //print the list
        for (Employee e: list) {
            System.out.println(e);
        }
        System.out.println("");
        //sort using names comparator
        Collections.sort(list,new EmployeeNameComparator());
        //print the list
        for (Employee e: list) {
            System.out.println(e);
        }
        System.out.println("");
        //sort using positions/salary comparator
        Collections.sort(list,new EmployeePositionSalaryComparator());
        //print the list
        for (Employee e: list) {
            System.out.println(e);
        }
        
        //sort using salary comparator
        System.out.println("\nSorted by Salary");
        Collections.sort(list,new EmployeeSalaryComparator());
        //print the list
        for (Employee e: list) {
            System.out.println(e);
        }
        
         //sort using years of Services comparator
        System.out.println("\nSorted by Years of Service");
        Collections.sort(list,new EmployeeYearsOfServiceComparator());
        //print the list
        for (Employee e: list) {
            System.out.println(e);
        }
        

    }

}
