package employee;

import user.User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mruth
 */
public class Employee implements Comparable<Employee> {

    private int empID;
    private String name;
    private int yearsOfService;
    private String position;
    private double salary;
    private User supervisor;

    public Employee() {
        this.empID = 9000;
        this.name = "Sonani Pankaj";
        this.position = "Solution Architech";
        this.yearsOfService = 15;
        this.salary = 110000;
        
    }
    public Employee(int empID, String name, int yearsOfService, String position, double salary) {
        this.empID = empID;
        this.name = name;
        this.yearsOfService = yearsOfService;
        this.position = position;
        this.salary = salary;
        
    }

    @Override
    public String toString() {
        return empID + "\t" + name + "\t" + yearsOfService + "\t" + position + "\t" + salary + "\t" + supervisor;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.empID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (this.empID != other.empID) {
            return false;
        }
        return true;
    }

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearsOfService() {
        return yearsOfService;
    }

    public void setYearsOfService(int yearsOfService) {
        this.yearsOfService = yearsOfService;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    
    public User getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(User supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public int compareTo(Employee other) {
        return this.empID - other.getEmpID();
    }

}
