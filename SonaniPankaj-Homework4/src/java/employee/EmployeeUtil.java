/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import javafx.application.Application;

/**
 *
 * @author psomani
 */
public class EmployeeUtil {

    //set up array for first names (first names of all new orleans saints)
    String[] fnames = {"Alex", "Terron", "Dan", "Chris", "J.T.", "Vonn", "Drew", "Jay",
        "Jermon", "Austin", "Will", "Kurt", "Brandon", "Ken", "Marcus",
        "Demario", "Tyeler", "Travin", "Trey", "Jayrone", "KeShun",
        "John", "Ted", "Garrett", "Woodrow", "Justin", "De'Vante",
        "Cory", "Josh", "Taysom", "Michael", "Josh", "Mark", "Natrell",
        "George", "Cameron", "Colton", "Alvin", "Hau'oli", "Keith",
        "A.J.", "Daniel", "Marshon", "Devaroe", "Rick", "Tommylee",
        "Zach", "Mitchell", "Wil", "Eldridge", "Arthur", "Henry",
        "Kamrin", "Thomas", "Al-Quadin", "David", "Andrus", "Ryan",
        "Sheldon", "Craig", "Patrick", "Tom", "Boston", "Tre'Quan",
        "Taylor", "Linden", "Nate", "Manti", "Mykkele", "Andrew",
        "Paul", "Landon", "Max", "Larry", "Ben", "P.J.", "Jonathan",
        "Deon"};
    //setup array for last names (last names of all US presidents)
    String[] lnames = {"Trump", "Obama", "Bush", "Clinton", "Reagan", "Carter", "Ford",
        "Nixon", "Johnson", "Kennedy", "Eisenhower", "Truman", "Roosevelt",
        "Hoover", "Coolidge", "Harding", "Wilson", "Taft", "McKinley",
        "Cleveland", "Harrison", "Arthur", "Garfield", "Hayes", "Grant",
        "Lincoln", "Buchanan", "Pierce", "Fillmore", "Taylor", "Polk",
        "Tyler", "Van Buren", "Jackson", "Adams", "Monroe", "Madison",
        "Jefferson", "Washington"};
    //setup array for positions -> positions/jobs on a pirate vessel
    String[] positions = {"Captain", "Navigator", "Quarternaster", "Boatswain",
        "Cooper", "Carpenter", "Doctor", "Master Gunner", "First Mate",
        "Sailor"};
    
    String[] supervisor = {"supervisors001","supervisors002","supervisors003"};

    Random random = new Random(); 
    public Set<String> positionSet = null;
    
    public ArrayList<Employee> getEmployeeList() {

        //create an array list that can only hold employees
        ArrayList<Employee> list = new ArrayList();
         positionSet = new HashSet();
        //need 50 of them
        for (int i = 0; i < 50; i++) {

            //employee ID -  6 digit number
            int empID = random.nextInt(89999) + 10000;
            //Name – String
            String name = lnames[random.nextInt(lnames.length)] + ", " + fnames[random.nextInt(fnames.length)];
            // Years of Service – int - 0 to 30
            int yos = random.nextInt(30);
            //Position – String 
            String position = positions[random.nextInt(positions.length)];
            //Salary – double 30,000 to 500,000
            double sal = random.nextDouble() * 470000 + 30000;

            String sup = supervisor[random.nextInt(supervisor.length)];
            //create an employee
            Employee e = new Employee(empID, name, yos, position, sal);
            
           
            //add to list
            list.add(e);
           positionSet.add(position);

        }        
        return list;
    }  
}
