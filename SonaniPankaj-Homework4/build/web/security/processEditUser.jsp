<%-- 
    Document   : processEditUser
    Created on : Jun 30, 2018, 6:16:02 PM
    Author     : pankajsonani
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="user.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    
    if (session.getAttribute("login") == null) {
        //user is logged in
        response.sendRedirect("login.jsp");

    } else {
        
        User user = (User)session.getAttribute("login");    
        ArrayList<User> users = (ArrayList<User>)application.getAttribute("users");        
        String un = "";        
        User u = null;        
        boolean allgood = true;
        
        if (request.getParameter("un") == null) {
            allgood = false;
        }
        
        if (allgood) {
            un = request.getParameter("un");
            u = new User();
            u.setUserName(un);
            if (users.contains(u)) {
                int index = users.indexOf(u);
                u = users.get(index);
            } else {
                allgood = false;
            }
        } 

        
        if (allgood) {
            if (!((user.equals(u)) || (user.getIsAdmin()))) {
                allgood = false;
            }
        }            
        
        if (!allgood) {
            response.sendRedirect("../index.jsp");
        } else {
            
            boolean error = false;
            
            if ((request.getParameter("pass1") == null) || (request.getParameter("pass1").length() <= 0)) {
                error = true;
                request.setAttribute("pass1", "notthere");
            }
            
            if ((request.getParameter("pass2") == null) || (request.getParameter("pass2").length() <= 0)) {
                error = true;
                request.setAttribute("pass2", "notthere");
            }
            
            String pass1 = request.getParameter("pass1");
            String pass2 = request.getParameter("pass2");
            
            if (!pass1.equals(pass2)) {
                error = true;
                request.setAttribute("pass1", "Passwords do not match");
            }
            
            if (user.getIsAdmin()) {
             
                if ((request.getParameter("position") == null) || (request.getParameter("position").length() <= 0)) {
                    error = true;
                    request.setAttribute("position", "notthere");
                }
                
            }
            
            if (error) {                
                request.getRequestDispatcher("editUser.jsp").forward(request, response);
                
            } else {
                u.setPassword(request.getParameter("pass1"));
                
                if (user.getIsAdmin()) {
                    u.setPosition(request.getParameter("position"));
                    
                    if (request.getParameter("administrator") != null) {
                        u.setIsAdmin(true);
                    } else {
                        //check that there is going to be at least one supervisor
                        boolean go = false;
                        //loop thru users
                        for (User check : users) {
                            if (!check.equals(u)) {
                                if (check.getIsAdmin()) {
                                    go = true;
                                }
                            }
                        }
                        
                        if (go) {
                            u.setIsAdmin(false);
                        }
                    }                    
                }  
                
                if (user.getIsAdmin()) {
                    response.sendRedirect("manageUsers.jsp");
                } else {
                    response.sendRedirect("../index.jsp");
                }
            }
            
        }
        
    
    }
%>