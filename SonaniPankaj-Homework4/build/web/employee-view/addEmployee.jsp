<%--
    Document   : addEmployee
    Created on : Jun 24, 2018, 11:17:46 AM
    Author     : psonani
--%>

<%@page import="user.User"%>
<%@page import="java.util.Set"%>
<%@page import="employee.Employee"%>
<%@page import="java.util.ArrayList"%>
<%@page import="employee.EmployeeUtil"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    String[] positions = {"Boatswain", "Captain", "Carpenter", "Cooper", "Doctor", "First Mate", "Master Gunner", "Navigator", "Quarternaster", "Sailor"};
    String[] supervisor = {"Supervisors 001","Supervisors 002","Supervisors 003",};
    if(session.getAttribute("login") == null) {
        response.sendRedirect("../security/login.jsp");
    } else {
        User user = (User)session.getAttribute("login");
        
%>
<html>
    <head>

        <title>Add New Employee</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/addEmployee.css">

        <script type="text/javascript">

            function validateForm() {
                var error = false;
                var message = "";

                var value = document.forms["form1"]["empID"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Employee ID cannot be black! \n";
                }

                value = document.forms["form1"]["name"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Name cannot be blank! \n";
                }

                value = document.forms["form1"]["years"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Yeas cannot be blank! \n";
                }

                value = document.forms["form1"]["position"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Position cannot be blank! \n";
                }

                value = document.forms["form1"]["salary"].value;
                if ((value == null) || (value.length <= 0)) {
                    error = true;
                    message = message + "Salary cannot be blank! \n";
                }

                if (error) {
                    alert(message);
                    return false;
                } else {
                    return true;
                }
            }

        </script>

    </head>
    <body>
        <h1>Add New Employee</h1>
        <div class="fullCard" id="thumbnail">
            <div class="cardContent">
                <div class="cardText">
                    <form method="GET" action="processAddEmployee.jsp" id="form1">
                        <table>
                            <tr>
                            <tr>
                                <% if (request.getParameter("empIDError") != null) {%>
                                <td><font color="red">Id(<%= request.getParameter("empIDError")%>):</font></td>
                                <td><input type="text" name="empID"></td>
                                    <% } else { %>
                                <td class="ed-header">Id</td>
                                <% if (request.getParameter("empID") != null) {%>
                                <td><input type="text" value="<%= request.getParameter("empID")%> " name="empID"></td>
                                    <% } else { %>
                                <td><input type="text" name="empID"></td>
                                    <% } %>
                                    <% } %>
                            </tr>
                            <tr>
                                <td class="ed-header">Name:</td>
                                <td><input type="text" name="name"></td>
                            </tr>
                            <tr>
                                <td class="ed-header">Years of Service</td>
                                <td><input type="text" name="years"></td>
                            </tr>
                            <tr>
                                <td class="ed-header">Positions:</td>
                                <td>
                                    <select name="position">
                                        <% for (String position : positions) { %>
                                        <% if (position.equals("Computer Science")) {%>
                                        <option value="<%= position%>" selected><%= position%></option>
                                        <% } else {%>
                                        <option value="<%= position%>" ><%= position%></option>
                                        <% } %>
                                        <% }%>

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="ed-header">Salary:</td>
                                <td><input type="text" name="salary"></td>
                            </tr>
                            <% if (user.getIsAdmin()) { %>
                            <tr>
                                <td class="ed-header">Supervisor</td>
                                <td>
                                    <select name="sup">
                                        <% for (String sup : supervisor) { %>
                                        <% if (sup.equals("")) {%>
                                        <option value="<%= sup%>" selected><%= sup%></option>
                                        <% } else {%>
                                        <option value="<%= sup%>" ><%= sup%></option>
                                        <% } %>
                                        <% }%>

                                    </select>
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td><input type="reset" value="Start Over?"></td>
                                <td><input type="submit" onclick="return validateForm()" value="Add Employee"></td>
                            </tr>
                            <tr>                                
                                <td><a class="aempid-link addEmp" href="${pageContext.request.contextPath}/index.jsp">Home</a></td>
                            </tr>
                        </table>                        
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<% } %>