<%-- 
    Document   : viewBySupervisor
    Created on : Jul 1, 2018, 9:41:04 PM
    Author     : pankajsonani
--%>

<%@page import="java.text.NumberFormat"%>
<%@page import="employee.EmployeeSalaryComparator"%>
<%@page import="employee.EmployeePositionComparator"%>
<%@page import="employee.EmployeeYearsOfServiceComparator"%>
<%@page import="employee.EmployeeNameComparator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="employee.Employee"%>
<%@page import="user.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("login") == null) {
        response.sendRedirect("../security/login.jsp");
    } else {

        User user = (User) session.getAttribute("login");

        ArrayList<Employee> empList = null;
        int empIdInt = 0;
        Employee employee = null;
        boolean allGood = true;
        ArrayList<Employee> showables = new ArrayList();
        //Check if employee list is not empty If empty than fill it.
        if (application.getAttribute("empListIndex") == null) {
            allGood = false;
        } else {
            empList = (ArrayList<Employee>) application.getAttribute("empListIndex");

            for (Employee emp : empList) {
                if (emp.getSupervisor().getRealName().equals(request.getParameter("supID"))) {
                    showables.add(emp);
                }
            }
        }
        if (request.getParameter("sort") == null) {
            Collections.sort(showables);
        } else {

            String sort = request.getParameter("sort");
            if (sort.equals("name")) {
                Collections.sort(showables, new EmployeeNameComparator());
            } else if (sort.equals("years")) {
                Collections.sort(showables, new EmployeeYearsOfServiceComparator());
            } else if (sort.equals("positions")) {
                Collections.sort(showables, new EmployeePositionComparator());
            } else {
                Collections.sort(showables, new EmployeeSalaryComparator());
            }
        }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/index.css">

        <title>Supervisor View</title>
    </head>
    <body>
        <h1>Supervisor View For <%= (request.getParameter("supID")) %> </h1>
        <table>
            <tr>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp">Id</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=name">Name</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=years">Years of Service</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=positions">Positions</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=salary">Salary</a></th>
               <% if (user.getIsAdmin()) { %>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/index.jsp?sort=name">Supervisor</a></th>
                <% } %>
                <th colspan="2">Actions</th>                
            </tr>
            <% for (Employee emp : showables) {%>
            <tr>
                <td><a class="aempid-link" href="${pageContext.request.contextPath}/employee-view/employeeDetail.jsp?empID=<%= emp.getEmpID()%>"><%= emp.getEmpID()%></a></td>            
                <td><%= emp.getName()%></td>
                <td><%= emp.getYearsOfService()%></td>
                <td><%= emp.getPosition()%></td>
                <% NumberFormat fmt = NumberFormat.getCurrencyInstance(); %>
                <td><%= fmt.format( emp.getSalary())%></td>
                <% if (user.getIsAdmin()) { %>
                <td><%= emp.getSupervisor().getRealName()%></td>
                <% } %>
                <td><a href="${pageContext.request.contextPath}/employee-view/editEmployee.jsp?empID=<%= emp.getEmpID()%>"><img height="20" width="20" src="${pageContext.request.contextPath}/images/edit.png" border="0"></a></td>
                <td><a href="${pageContext.request.contextPath}/employee-view/deleteEmployee.jsp?empID=<%= emp.getEmpID()%>"><img height="20" width="20" src="${pageContext.request.contextPath}/images/delete3.png" border="0"></a></td>
            </tr>
            <% }%>
            <tr>
               <td><a class="aempid-link addEmp" href="${pageContext.request.contextPath}/index.jsp">Home</a></td>
            </tr>
        </table>
    </body>
</html>
<% }%>