<%-- 
    Document   : index
    Created on : Jun 23, 2018, 1:27:01 PM
    Author     : psonani
--%>

<%@page import="employee.EmployeeSalaryComparator"%>
<%@page import="employee.EmployeePositionComparator"%>
<%@page import="employee.EmployeeYearsOfServiceComparator"%>
<%@page import="employee.EmployeeNameComparator"%>
<%@page import="employee.Employee"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="employee.EmployeeUtil"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
   if (session.getAttribute("login") == null) {
        //user is already logged in
        response.sendRedirect("login.jsp");

    } else {

    ArrayList<Employee> empListIndex = null;

    if (application.getAttribute("empListIndex") != null) {
        empListIndex = (ArrayList<Employee>) application.getAttribute("empListIndex");
    } else {
        EmployeeUtil employeeUtil = new EmployeeUtil();
        empListIndex = employeeUtil.getEmployeeList();       

        application.setAttribute("empListIndex", empListIndex);
    }

    if(request.getParameter("sort") == null) {
        Collections.sort(empListIndex);
    } else {
    
        String sort = request.getParameter("sort");
        if(sort.equals("name")){
            Collections.sort(empListIndex, new EmployeeNameComparator());
        } else if (sort.equals("years")){
            Collections.sort(empListIndex, new EmployeeYearsOfServiceComparator());
        } else if (sort.equals("positions")){
            Collections.sort(empListIndex, new EmployeePositionComparator());
        } else  {
            Collections.sort(empListIndex, new EmployeeSalaryComparator());
        }
    }
    
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/index.css">
        <title>Employee View</title>
    </head>
    <body>
        <h1>Sortable Employee Detail </h1>      
        <table>
            <tr>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeeSortableList.jsp">Id</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeeSortableList.jsp?sort=name">Name</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeeSortableList.jsp?sort=years">Years of Service</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeeSortableList.jsp?sort=position">Position</a></th>
                <th><a class="emp-table-header" href="${pageContext.request.contextPath}/employee-view/employeeSortableList.jsp?sort=salary">Salary</a></th>                               
            </tr>
            <% for (Employee emp : empListIndex) {%>
            <tr>    
                <td><%= emp.getEmpID() %></td>
                <td><%= emp.getName() %></td>
                <td><%= emp.getYearsOfService() %></td>
                <td><%= emp.getPosition() %></td>
                 <%
                    int temp = (int) emp.getSalary();
                    double salary = temp / 100.0;
                %>
                <td><%= salary %></td>
               
            </tr>
            <% }%>
        </table>
        <br>
        <a class="aempid-link addEmp" href="${pageContext.request.contextPath}/index.jsp">Home</a> 
        <a class="aempid-link addEmp" href="${pageContext.request.contextPath}/employee-view/addEmployee.jsp">Add Employee</a>
         
    </body>
</html>
<% } %>