<%-- 
    Document   : processEditEmployee
    Created on : Jun 25, 2018, 2:24:05 PM
    Author     : psonani
--%>

<%@page import="user.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="employee.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if (session.getAttribute("login") == null) {
        //user is already logged in
        response.sendRedirect("login.jsp");

    } else {
        User user = (User) session.getAttribute("login");
        ArrayList<Employee> empList = null;
        int empIdInt = 0;
        Employee employee = null;
        boolean allGood = true;

        //Check if employee list is not empty If empty than fill it.
        if (application.getAttribute("empListIndex") == null) {
            allGood = false;
        } else {
            empList = (ArrayList<Employee>) application.getAttribute("empListIndex");
        }

        //Check id empID is not null
        if (request.getParameter("empID") == null) {
            allGood = false;
        }

        //If parse empID into int
        if (allGood) {
            String empIdStr = request.getParameter("empID");
            try {
                empIdInt = Integer.parseInt(empIdStr);
            } catch (Exception e) {
                allGood = false;
            }
        }

        //check for empID is there
        if (allGood) {
            employee = new Employee();
            employee.setEmpID(empIdInt);

            if (empList.contains(employee)) {
                int index = empList.indexOf(employee);
                employee = empList.get(index);
            } else {
                allGood = false;
            }
        }

        if (allGood) {
            if (((employee.getSupervisor().equals(user.getRealName())) || (user.getIsAdmin()))) {
                allGood = true;
            } else {
                allGood = false;
            }
        }

        // If there is error redirect into index.jsp
        if (!allGood) {
            response.sendRedirect("../index.jsp");
        } else {
            boolean error = false;
            //check Name
            if ((request.getParameter("name") == null) || (request.getParameter("name").length() <= 0)) {
                error = true;
                request.setAttribute("nameError", "notthere");
            }
            //Check Years of service
            if (request.getParameter("years") != null) {
                try {
                    Integer.parseInt(request.getParameter("years"));
                } catch (Exception e) {
                    error = true;
                    request.setAttribute("yearsError", "notanint");
                }
            } else {
                error = true;
                request.setAttribute("yearsError", "notthere");
            }
            //check Position
            if ((request.getParameter("position") == null) || (request.getParameter("position").length() <= 0)) {
                error = true;
                request.setAttribute("positionError", "notthere");
            }
            //Check Salary
            if (request.getParameter("salary") != null) {
                try {
                    Double.parseDouble(request.getParameter("salary"));
                } catch (Exception e) {
                    request.setAttribute("salaryError", "notadouble");
                    error = true;
                }
            } else {
                error = true;
                request.setAttribute("salaryError", "notthere");
            }

            if (error) {

                request.getRequestDispatcher("editEmployee.jsp").forward(request, response);

            } else {

                ArrayList<User> users = (ArrayList<User>) application.getAttribute("users");
                //add Employee
                employee.setName(request.getParameter("name"));
                employee.setYearsOfService(Integer.parseInt(request.getParameter("years")));
                employee.setPosition(request.getParameter("position"));
                employee.setSalary(Double.parseDouble(request.getParameter("salary")));
                if (user.getIsAdmin()) {

                    employee.setSupervisor(users.get(0));
                } else {

                }

                response.sendRedirect("employeeDetail.jsp?empID=" + employee.getEmpID());
            }
        }

%>
